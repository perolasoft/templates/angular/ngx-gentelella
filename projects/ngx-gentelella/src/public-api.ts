/*
 * Public API Surface of ngx-gentelella
 */

export * from './lib/ngx-gentelella.service';
export * from './lib/ngx-gentelella.component';
export * from './lib/ngx-gentelella.module';
